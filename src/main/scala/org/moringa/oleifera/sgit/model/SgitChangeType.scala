package org.moringa.oleifera.sgit.model

import org.eclipse.jgit.diff.DiffEntry

object SgitChangeType extends Enumeration {
  type SgitChangeType = Value

  val Add, Copy, Delete, Modify, Rename = Value
  //	ADD(1), COPY(2), DELETE(3), MODIFY(4), RENAME(5)

  def getSgitChangeTypeFromType(typeValue: Int): SgitChangeType = {
    typeValue match {
      case 1 => Add
      case 2 => Copy
      case 3 => Delete
      case 4 => Modify
      case 5 => Rename
    }
  }

  def getSgitChangeTypeStrFromType(typeValue: Int): String = {
    typeValue match {
      case 1 => "ADD"
      case 2 => "COPY"
      case 3 => "DELETE"
      case 4 => "MODIFY"
      case 5 => "RENAME"
    }
  }

  def getSgitChangeTypeFromChangeType(changeType: DiffEntry.ChangeType): SgitChangeType = {
    changeType match {
      case DiffEntry.ChangeType.ADD => Add
      case DiffEntry.ChangeType.COPY => Copy
      case DiffEntry.ChangeType.DELETE => Delete
      case DiffEntry.ChangeType.MODIFY => Modify
      case DiffEntry.ChangeType.RENAME => Rename
    }
  }

  def getTypeFromChangeType(changeType: DiffEntry.ChangeType): Int = {
    changeType match {
      case DiffEntry.ChangeType.ADD => 1
      case DiffEntry.ChangeType.COPY => 2
      case DiffEntry.ChangeType.DELETE => 3
      case DiffEntry.ChangeType.MODIFY => 4
      case DiffEntry.ChangeType.RENAME => 5
    }
  }

  def getTypeFromSgitChangeType(changeType: SgitChangeType): Int = {
    changeType match {
      case Add => 1
      case Copy => 2
      case Delete => 3
      case Modify => 4
      case Rename => 5
    }
  }
}
