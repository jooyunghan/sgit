package org.moringa.oleifera.sgit.model

import scala.collection.mutable.HashMap

class MemoryCache {

  var changeTypeMap: HashMap[String, Int] = null
  var pathMap: HashMap[String, Int] = null
  var fileMap: HashMap[String, Int] = null
}


object MemoryCache {
}