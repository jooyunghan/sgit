package org.moringa.oleifera.sgit.model

import java.sql.Connection
import java.sql.DriverManager

class DBConnection(u: String, i: String, p: String) {
  var url = u
  var id = i
  var pw = p
  
  var conn: Connection = null
  
  def connect() {
    Class.forName("org.postgresql.Driver");
    
    conn = DriverManager.getConnection(url, id, pw);
  }

  def close() {
    conn.close
  }
}

object DBConnection {
  def connectDB(serverNdb: String): Connection = {
    Class.forName("org.postgresql.Driver");
    DriverManager.getConnection("jdbc:postgresql://" + serverNdb, "agile", "scrum");
  }
}