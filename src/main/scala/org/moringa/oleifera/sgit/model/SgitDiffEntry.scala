package org.moringa.oleifera.sgit.model

import org.eclipse.jgit.diff.DiffFormatter
import org.eclipse.jgit.diff.RawTextComparator
import org.eclipse.jgit.util.io.DisabledOutputStream
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.revwalk.RevCommit
import org.eclipse.jgit.diff.DiffEntry
import java.io.FileOutputStream
import java.io.File
import scala.collection.JavaConverters._
import scala.collection.mutable.HashMap

object SgitDiffEntry {

  val basePath = "base"
  val prevPath = "prev"
  val nextPath = "next"

  def clearDirectory(base: File) {
    def clearRecursive(file: File) {
      if (file.isFile) file.delete
      else if (file.isDirectory) {
        file.listFiles.toList.foreach(clearRecursive)
        file.delete
      } else println("huk<recur>: " + file)
    }
    if (base.isFile) base.delete
    else if (base.isDirectory) base.listFiles.toList.foreach(clearRecursive)
    else println("huk: " + base)
  }

  def getSgitDiffEntries(repository: Repository, commit: RevCommit, parent: RevCommit): List[SgitDiffEntry] = {

    clearDirectory(new File(basePath))

    val diffFormatter = new DiffFormatter(DisabledOutputStream.INSTANCE)
    diffFormatter.setRepository(repository)
    diffFormatter.setDiffComparator(RawTextComparator.DEFAULT)
    diffFormatter.setDetectRenames(true)

    val diffEntries = diffFormatter.scan(parent.getTree, commit.getTree).asScala
    val sgitDiffEntries = diffEntries.map(diffEntry => {
      SgitDiffEntry.getSgitDiffEntry(repository, diffEntry)
    })

    sgitDiffEntries.toList
  }

  def getSgitDiffEntry(repository: Repository, diffEntry: DiffEntry): SgitDiffEntry = {

    val sgitChangeType = SgitChangeType.getSgitChangeTypeFromChangeType(diffEntry.getChangeType)
    val typeValue = SgitChangeType.getTypeFromChangeType(diffEntry.getChangeType)

    val filePath = diffEntry.getNewPath
    val (path, file) = filePath.lastIndexOf("/") match {
      case -1 => ("", filePath)
      case index => filePath.splitAt(index + 1)
    }

    val sigFunctionComplexities = SgitFunctionComplexity.getSgitFunctionComplexities(repository, basePath, prevPath, nextPath, diffEntry)

    new SgitDiffEntry(typeValue, path, file, sigFunctionComplexities)
  }
}

class SgitDiffEntry(changeType: Int, path: String, file: String, functionComplexities: List[SgitFunctionComplexity]) {

  def getChangeType = changeType
  def getPath = path
  def getFile = file
  def getFunctionComplexities = functionComplexities
}
