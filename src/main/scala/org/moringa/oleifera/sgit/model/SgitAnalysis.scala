package org.moringa.oleifera.sgit.model

import java.text.SimpleDateFormat
import java.util.Date

import scala.collection.JavaConversions

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.revwalk.RevCommit

object SgitAnalysis {

  def getSgitAnalysis(repository: Repository): SgitAnalysis = {
    val git = new Git(repository)
    val commits = getCommits(repository)

    val commitsFilteredWithDate = commits.filter( commit => {
      val date = commit.getAuthorIdent.getWhen
      val simpleFormat = new SimpleDateFormat("yyyy-MM-dd")

      date.after(simpleFormat.parse("2013-09-01"))
    })

    val sgitCommits = SgitCommit.getSgitCommits(repository, commitsFilteredWithDate)

    new SgitAnalysis(repository.getDirectory.getCanonicalPath, new Date, sgitCommits)
  }

  def getCommits(repository: Repository): List[RevCommit] = {
    val git = new Git(repository)
    val commits = git.log.all.call
    JavaConversions.asScalaIterable(commits).toList
  }
}

class SgitAnalysis(repositoryName: String, date: Date, sgitCommits: List[SgitCommit]) {

  def getRepositoryName = repositoryName
  def getDate = date
  def getSgitCommits = sgitCommits
}
