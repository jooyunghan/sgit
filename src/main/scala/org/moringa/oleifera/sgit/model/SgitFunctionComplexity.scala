package org.moringa.oleifera.sgit.model

import java.io.File
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import scala.sys.process.stringToProcess
import org.eclipse.jgit.diff.DiffEntry
import org.eclipse.jgit.lib.Repository
import java.io.FileOutputStream
import com.sun.org.apache.xpath.internal.functions.Function
import org.eclipse.jgit.lib.ObjectId

object SgitFunctionComplexity {

  def getSgitFunctionComplexities(repository: Repository, basePath: String, prevPath: String, nextPath: String, diffEntry: DiffEntry): List[SgitFunctionComplexity] = {

    def extractFunctionComplexities(subPath: String, filePath: String): HashMap[String, Int] = {
      val udbPath = "./" + basePath + File.separator + subPath + ".udb"
      val localFilePath = "./" + basePath + File.separator + subPath + File.separator + filePath
      val und = "und -quiet create -db " + udbPath + " -languages c++ java add -file " + localFilePath + " analyze -all" !

      val result = "uperl sgit.pl " + subPath!!

      def getFunctionCc(str: String): HashMap[String, Int] = {
        val hashMap = new HashMap[String, Int]
        val list = str.split("\n")

        def getFunctionAndCC(line: String) {

          val p = """\s*([\w()~=\+!:_]+)-(\d+)\s*""".r
          try {
            val p(func, cc) = line
            hashMap += ((func.toString, cc.toInt))
          } catch {
            case e: Exception => println("not: " + line)
          }
        }

        list.foreach(getFunctionAndCC)
        hashMap
      }

      getFunctionCc(result)
    }

    def getFunctionComplexities(subPath: String, filePath: String, objectId: ObjectId): HashMap[String, Int] = {

      var map: HashMap[String, Int] = null

      try {
        if (!filePath.eq("/dev/null")) {
          val newLoader = repository.open(objectId)

          val file = new File(basePath + File.separator + subPath, filePath)
          new File(file.getParent).mkdirs
          val fileOutputStream = new FileOutputStream(file)

          newLoader.copyTo(fileOutputStream)
          fileOutputStream.close
          map = extractFunctionComplexities(subPath, filePath)
        }
      } catch {
        case e: Exception =>
          println(subPath + " lost" + filePath)
          //        e.printStackTrace
          return null
      }
      map
    }

    def combineSgitFunctionComplexities(prevMap: HashMap[String, Int], nextMap: HashMap[String, Int]): List[SgitFunctionComplexity] = {
//      println("prevMap: " + prevMap)
//      println("nextMap: " + nextMap)

      def getUnionSgitFunctionComplexities: List[SgitFunctionComplexity] = {
        val sgitFunctionComplexitiesListBuffer = new ListBuffer[SgitFunctionComplexity]

        prevMap.keys.toList.map(function => {
          nextMap.contains(function) match {
            case false => sgitFunctionComplexitiesListBuffer.+=(new SgitFunctionComplexity(function, Some(prevMap(function)), None))
            case true => sgitFunctionComplexitiesListBuffer.+=(new SgitFunctionComplexity(function, Some(prevMap(function)), Some(nextMap(function))))
          }
        })

        nextMap.keys.toList.map(function => {
          prevMap.contains(function) match {
            case false => sgitFunctionComplexitiesListBuffer += new SgitFunctionComplexity(function, None, Some(nextMap(function)))
            case true =>
          }
        })

        sgitFunctionComplexitiesListBuffer.toList
      }

      if (prevMap != null && nextMap != null) getUnionSgitFunctionComplexities
      else {
        val listBuffer = new ListBuffer[SgitFunctionComplexity]
        if (prevMap != null) {
          listBuffer ++= prevMap.keys.toList.map(function => new SgitFunctionComplexity(function, Some(prevMap(function)), None))
        }
        if (nextMap != null) {
          listBuffer ++= nextMap.keys.toList.map(function => new SgitFunctionComplexity(function, None, Some(nextMap(function))))
        }
        listBuffer.toList
      }
    }

    combineSgitFunctionComplexities(getFunctionComplexities(prevPath, diffEntry.getOldPath, diffEntry.getOldId.toObjectId),
      getFunctionComplexities(nextPath, diffEntry.getNewPath, diffEntry.getNewId.toObjectId)).toList
  }
}

class SgitFunctionComplexity(function: String, prevComplexity: Option[Int], nextComplexity: Option[Int]) {

  def getFunction = function
  def getPrevComplexity = prevComplexity
  def getNextComplexity = nextComplexity
}
