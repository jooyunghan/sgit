package org.moringa.oleifera.sgit

import java.io.File
import java.sql.Connection
import scala.collection.mutable.HashMap
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.moringa.oleifera.sgit.model.DBConnection
import org.moringa.oleifera.sgit.model.SgitChangeType
import org.moringa.oleifera.sgit.model.DataAgent
import org.moringa.oleifera.sgit.model.SgitAnalysis
import org.moringa.oleifera.sgit.model.SgitFunctionComplexity
import org.moringa.oleifera.sgit.model.SgitCommit
import org.moringa.oleifera.sgit.model.SgitDiffEntry

class HotSpotInSourceCode(dbServer: String, dbName: String, repositoryName: String) {

  val builder = new FileRepositoryBuilder
  val repository = builder.setGitDir(new File(repositoryName)).readEnvironment().findGitDir().build()

  var dbConnection: Connection = null
  val dataAgent: DataAgent = new DataAgent(dbServer, dbName)

  var changeTypeMap: HashMap[String, Int] = null
  var pathMap: HashMap[String, Int] = null
  var fileMap: HashMap[String, Int] = null

  def analyze {

    initMemCache

    isValidRepository match {
      case false => {
        println("repository corrupt")
        return
      }
      case true =>
    }

    val analysis = getSgitAnalysis
    //    analysis.getSgitCommits.foreach(commit => {
    //      println(commit.getObjectId)
    //      commit.getDiffEntries.foreach(diffEntry => {
    //        println("\t" + diffEntry.getChangeType + ", " + diffEntry.getPath + diffEntry.getFile)
    //        diffEntry.getSgitFunctionComplexities.foreach(functionComplexity => {
    //          println("\t\t" + functionComplexity.getFunction + "~" + functionComplexity.getPrevComplexity + ", " + functionComplexity.getNextComplexity)
    //        })
    //      })
    //    })

    var out_file = new java.io.FileOutputStream("analysis.result")
    var out_stream = new java.io.PrintStream(out_file)
    analysis.getSgitCommits.foreach(commit => {
      out_stream.println(commit.getObjectId)
      commit.getDiffEntries.foreach(diffEntry => {
        out_stream.println("\t" + diffEntry.getChangeType + ", " + diffEntry.getPath + diffEntry.getFile)
        diffEntry.getFunctionComplexities.foreach(functionComplexity => {
          out_stream.println("\t\t" + functionComplexity.getFunction + "~" + functionComplexity.getPrevComplexity + ", " + functionComplexity.getNextComplexity)
          //          if( functionComplexity.getNextComplexity != None && functionComplexity.getPrevComplexity != None &&
          //              functionComplexity.getNextComplexity.get != functionComplexity.getPrevComplexity.get)
          //        	  out_stream.println("\t\t\t" + functionComplexity.getPrevComplexity + ", " + functionComplexity.getNextComplexity)

        })
      })
    })
    out_stream.close

    val result = insertSgitAnalysis(analysis)
    println("result: " + result)

    dbConnection = DBConnection.connectDB(dbServer + "/" + dbName)
    dbConnection.close
  }

  def insertSgitAnalysis(sgitAnalysis: SgitAnalysis): Boolean = {
    val result = dataAgent.insertSgitAnalysis(sgitAnalysis.getRepositoryName, sgitAnalysis.getDate)

    def insertSgitCommit(sgitAnalysisKey: Int, sgitCommit: SgitCommit): Boolean = {
      val result = dataAgent.insertSgitCommit(sgitAnalysisKey, sgitCommit)

      def insertSgitDiffEntry(sgitCommitKey: Int, sgitDiffEntry: SgitDiffEntry): Boolean = {
        val changeTypeKey = getChangeType(sgitDiffEntry.getChangeType).get
        val pathKey = getPathName(sgitDiffEntry.getPath).get
        val fileKey = getFileName(sgitDiffEntry.getFile).get

        val result = dataAgent.insertSgitDiffEntry(sgitCommitKey, changeTypeKey, pathKey, fileKey)

        def insertSgitFunctionComplexity(sgitDiffEntryKey: Int, sgitFunctionComplexity: SgitFunctionComplexity): Boolean = {
          val result = dataAgent.insertSgitFunctionComplexity(sgitDiffEntryKey, sgitFunctionComplexity.getFunction,
            sgitFunctionComplexity.getPrevComplexity, sgitFunctionComplexity.getNextComplexity)

          result match {
            case Some(functionComplexityKey) => true
            case None => false
          }
        }

        result match {
          case Some(diffEntryKey) => sgitDiffEntry.getFunctionComplexities.forall(insertSgitFunctionComplexity(diffEntryKey, _))
          case None => false
        }
      }

      result match {
        case Some(commitKey) => sgitCommit.getDiffEntries.forall(insertSgitDiffEntry(commitKey, _))
        case None => false
      }
    }

    result match {
      case Some(analysisKey) => sgitAnalysis.getSgitCommits.forall(insertSgitCommit(analysisKey, _))
      case None => false
    }
  }

  def initMemCache {
    changeTypeMap = dataAgent.selectSgitChangeType
    pathMap = dataAgent.selectSgitPath
    fileMap = dataAgent.selectSgitFile
  }

  def getPathName(pathName: String): Option[Int] = {
    pathMap.contains(pathName) match {
      case true => pathMap.get(pathName)
      case false => {
        val result = dataAgent.insertPathName(pathName)

        result match {
          case Some(pKey) => {
            pathMap.put(pathName, result.get)
            result
          }
          case None => None
        }
      }
    }
  }

  def getFileName(fileName: String): Option[Int] = {
    fileMap.contains(fileName) match {
      case true => fileMap.get(fileName)
      case false => {
        val result = dataAgent.insertFileName(fileName)

        result match {
          case Some(pKey) => {
            fileMap.put(fileName, result.get)
            result
          }
          case None => None
        }
      }
    }
  }

  def getChangeType(changeTypeConst: Int): Option[Int] = {
    val changeTypeStr = SgitChangeType.getSgitChangeTypeStrFromType(changeTypeConst)

    changeTypeMap.contains(changeTypeStr) match {
      case true => changeTypeMap.get(changeTypeStr)
      case false => {
        val result = dataAgent.insertChangeType(changeTypeStr)
        result match {
          case Some(pKey) => {
            changeTypeMap.put(changeTypeStr, result.get)
            result
          }
          case None => None
        }
      }
    }
  }

  def isValidRepository: Boolean = {
    val git = new Git(repository)

    try {
      val revWalk = new RevWalk(repository)
      val commits = git.log.all.call
    } catch {
      case e: Exception => {
        e.printStackTrace()
        return false
      }
    }
    true
  }

  def getSgitAnalysis: SgitAnalysis = {
    val sgitAnalysis = SgitAnalysis.getSgitAnalysis(repository)

    sgitAnalysis
  }

}