﻿-- Table: analysis

-- DROP TABLE analysis;

CREATE TABLE analysis
(
  analysis_id serial NOT NULL,
  repository_name character varying,
  analysis_date date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE analysis
  OWNER TO agile;



-- Table: change_type

-- DROP TABLE change_type;

CREATE TABLE change_type
(
  change_type_id serial NOT NULL,
  change_type_name character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE change_type
  OWNER TO agile;



-- Table: commit

-- DROP TABLE commit;

CREATE TABLE commit
(
  commit_id serial NOT NULL,
  commit_object_id character varying,
  analysis_id integer,
  author character varying,
  committer character varying,
  commit_date date,
  commit_msg character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE commit
  OWNER TO agile;



-- Table: commit_complexity

-- DROP TABLE commit_complexity;

CREATE TABLE commit_complexity
(
  commit_complexity_id serial NOT NULL,
  commit_id integer,
  previous_complexity integer,
  next_complexity integer,
  function character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE commit_complexity
  OWNER TO agile;




-- Table: diff_entry

-- DROP TABLE diff_entry;

CREATE TABLE diff_entry
(
  diff_entry_id serial NOT NULL,
  commit_id integer,
  change_type_id integer,
  path_id integer,
  file_id integer
)
WITH (
  OIDS=FALSE
);
ALTER TABLE diff_entry
  OWNER TO agile;


-- Table: file

-- DROP TABLE file;

CREATE TABLE file
(
  file_id serial NOT NULL,
  file_name character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE file
  OWNER TO agile;




-- Table: function_complexity

-- DROP TABLE function_complexity;

CREATE TABLE function_complexity
(
  function_complexity_id serial NOT NULL,
  diff_entry_id integer,
  function_name character varying,
  previous_complexity integer,
  next_complexity integer
)
WITH (
  OIDS=FALSE
);
ALTER TABLE function_complexity
  OWNER TO agile;




-- Table: path

-- DROP TABLE path;

CREATE TABLE path
(
  path_id serial NOT NULL,
  path_name character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE path
  OWNER TO agile;
