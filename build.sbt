name := "S GIT"
 
version := "0.1"
 
scalaVersion := "2.10.2"

resolvers ++= Seq(
  "junit interface repo" at "https://repository.jboss.org/nexus/content/repositories/scala-tools-releases",
  "releases"  at "http://oss.sonatype.org/content/repositories/releases",
  "snapshots" at "http://oss.sonatype.org/content/repositories/snapshots",
  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Maven II" at "http://mvnrepository.com/artifact/",
  "scala releases"  at "https://oss.sonatype.org/content/groups/scala-tools/"
)

libraryDependencies ++= Seq(
  "org.specs2" %% "specs2" % "1.12.3" % "test",
  "org.specs2" %% "specs2-scalaz-core" % "6.0.1" % "test",
  "org.scalatest" %% "scalatest" % "1.9.1",
  "org.scalacheck" %% "scalacheck" % "1.10.1",
  "com.novocode" % "junit-interface" % "0.9" % "test",
  "junit" % "junit" % "4.10" % "test",
  "org.mongodb" %% "casbah" % "2.5.0",
  "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
  "net.sf.opencsv" % "opencsv" % "2.1",
  "joda-time" % "joda-time" % "2.1",
  "org.joda" % "joda-convert" % "1.3",
  "org.eclipse.jgit" % "org.eclipse.jgit" % "3.0.0.201306101825-r",
/*
  "org.scalaj" %% "scalaj-time" % "0.6",
  "log4j" % "log4j" % "1.2.15" excludeAll(
    ExclusionRule(organization = "com.sun.jdmk"),
    ExclusionRule(organization = "com.sun.jmx"),
    ExclusionRule(organization = "javax.jms")
  ),
*/
  "org.specs2" % "specs2_2.10" % "1.14" % "test",
  "org.scalatest" % "scalatest_2.10" % "1.9.1",
  "org.scalacheck" % "scalacheck_2.10" % "1.10.1",
  "junit" % "junit" % "4.11" % "test",
  "joda-time" % "joda-time" % "1.6",
  "log4j" % "log4j" % "1.2.17",
  "com.typesafe.akka" % "akka-actor_2.10" % "2.1.4",
  "org.scala-lang" % "scala-compiler" % "2.10.2",
  "org.scala-lang" % "scala-reflect" % "2.10.2",
  "org.scala-lang" % "scala-actors" % "2.10.2",
  "org.scala-lang" % "scala-swing" % "2.10.0"
)
