
library("RPostgreSQL")
library(ca)

drv <- dbDriver("PostgreSQL")
conn <- dbConnect(drv, host="localhost", dbname="sgit", user="agile", password="scrum")

begin_time <- Sys.time()

#######################################################################################
##

query <- paste("
select
  a_table.analysis_id, a_table.repository_name, a_table.analysis_date,
	c_table.author, c_table.committer, c_table.commit_date, c_table.commit_object_id, c_table.commit_msg,
	ct_table.change_type_name, 
	p_table.path_name,
	f_table.file_name
from 
	diff_entry as de_table
	left outer join commit as c_table
	on de_table.commit_id = c_table.commit_id
	left outer join analysis as a_table
	on c_table.analysis_id = a_table.analysis_id
	left outer join change_type as ct_table
	on de_table.change_type_id = ct_table.change_type_id
	left outer join path as p_table
	on de_table.path_id = p_table.path_id
	left outer join file as f_table
	on de_table.file_id = f_table.file_id
where
	f_table.file_name like '%'
	and p_table.path_name not like '/%'
", ";",sep="")

query <- "
select
  a_table.repository_name, a_table.analysis_date,
	c_table.commit_id, c_table.commit_date, c_table.commit_msg,
	c_table.author,
	p_table.path_id, p_table.path_name
from 
	commit as c_table
	left outer join analysis as a_table
	on c_table.analysis_id = a_table.analysis_id
	left outer join diff_entry as de_table
	on c_table.commit_id = de_table.commit_id
	left outer join change_type as ct_table
	on de_table.change_type_id = ct_table.change_type_id
	left outer join path as p_table
	on de_table.path_id = p_table.path_id
	left outer join file as f_table
	on de_table.file_id = f_table.file_id
where
	f_table.file_name like '%.java'
	and p_table.path_name not like '/%'
--	and ct_table.change_type_name in ('MODIFY', 'ADD')	--	'RENAME', 'DELETE', 'COPY'
group by 
	a_table.repository_name, a_table.analysis_date,
	c_table.commit_id, c_table.author, c_table.commit_date, c_table.commit_msg,
	p_table.path_id, p_table.path_name
;
"

query <- "
select
  a_t.repository_name, a_t.analysis_date,
c_t.commit_id, c_t.commit_date, c_t.commit_msg,
c_t.author,
p_t.path_id, p_t.path_name
from 
commit as c_t
left outer join analysis as a_t
on c_t.analysis_id = a_t.analysis_id
left outer join diff_entry as de_t
on c_t.commit_id = de_t.commit_id
left outer join change_type as ct_t
on de_t.change_type_id = ct_t.change_type_id
left outer join path as p_t
on de_t.path_id = p_t.path_id
left outer join file as f_t
on de_t.file_id = f_t.file_id
where
(
f_t.file_name like '%.h'
or f_t.file_name like '%.hpp'
or f_t.file_name like '%.c'
or f_t.file_name like '%.cpp'
)
and p_t.path_name not like '/%'
and c_t.commit_date >= '2013-10-01' and c_t.commit_date < '2013-11-01' 
--  and c_t.commit_date > '2013-06-01' and c_t.commit_date < '2013-06-30' 
--  and c_t.commit_date > '2013-07-01' and c_t.commit_date < '2013-07-31' 
--  and c_t.commit_date > '2013-08-01' and c_t.commit_date < '2013-08-31' 
--  and c_t.commit_date > '2013-09-01' and c_t.commit_date < '2013-09-30' 
--	and ct_t.change_type_name in ('MODIFY', 'ADD')	--	'RENAME', 'DELETE', 'COPY'
group by 
a_t.repository_name, a_t.analysis_date,
c_t.commit_id, c_t.author, c_t.commit_date, c_t.commit_msg,
p_t.path_id, p_t.path_name
;"

query <- "
select
a_t.repository_name, a_t.analysis_date,
c_t.commit_id, c_t.commit_date, c_t.commit_msg,
c_t.author,
p_t.path_id, p_t.path_name
from 
commit as c_t
left outer join analysis as a_t
on c_t.analysis_id = a_t.analysis_id
left outer join diff_entry as de_t
on c_t.commit_id = de_t.commit_id
left outer join change_type as ct_t
on de_t.change_type_id = ct_t.change_type_id
left outer join path as p_t
on de_t.path_id = p_t.path_id
left outer join file as f_t
on de_t.file_id = f_t.file_id
where
(
f_t.file_name like '%.h'
or f_t.file_name like '%.hpp'
or f_t.file_name like '%.c'
or f_t.file_name like '%.cpp'
)
and p_t.path_name not like '/%'
and c_t.commit_date >= '2013-10-01' and c_t.commit_date < '2013-11-01' 
--  and c_t.commit_date > '2013-06-01' and c_t.commit_date < '2013-06-30' 
--  and c_t.commit_date > '2013-07-01' and c_t.commit_date < '2013-07-31' 
--  and c_t.commit_date > '2013-08-01' and c_t.commit_date < '2013-08-31' 
--  and c_t.commit_date > '2013-09-01' and c_t.commit_date < '2013-09-30' 
--  and ct_t.change_type_name in ('MODIFY', 'ADD')	--	'RENAME', 'DELETE', 'COPY'
group by 
a_t.repository_name, a_t.analysis_date,
c_t.commit_id, c_t.author, c_t.commit_date, c_t.commit_msg,
p_t.path_id, p_t.path_name
;"

sgit.raw <- dbGetQuery(conn, query)

# sgit.raw$author

col_names <- colnames(sgit.raw)
sgit.raw[,ncol(sgit.raw) + 1] = paste(sgit.raw$path, sgit.raw$file, sep="")
col_names <- c(col_names, "full_file")
colnames(sgit.raw) <- col_names

# dimnames(sgit.raw)
# dim(sgit.raw)

sgit.raw.date_max <- max(sgit.raw$commit_date)
sgit.raw.date_min <- min(sgit.raw$commit_date)

sgit.raw.date_max
sgit.raw.date_min

during <- as.integer((sgit.raw.date_max - sgit.raw.date_min) / 7)
# during

week_info <- seq(sgit.raw.date_min, length.out = during, by = "1 week")

# summary(sgit.raw)
# colnames(sgit.raw)
# table(sgit.raw$author, sgit.raw$committer)

# relation_table_between_author_and_path <- table(sgit.raw$author, sgit.raw$path_name)
# relation_table_between_author_and_path <- table(sgit.raw$author, sgit.raw$full_file)
relation_table_between_author_and_path <- table(sgit.raw$author, sgit.raw$path_id)
relation_table_between_author_and_path
dim(relation_table_between_author_and_path)
relation_table_between_author_and_path_ca <- ca(relation_table_between_author_and_path)
plot(relation_table_between_author_and_path_ca)

##
#######################################################################################

end_time <- Sys.time()
simulated_time = end_time - begin_time
print(paste("Begin time: ", begin_time, ", End time: ", end_time, "Simulated time: ", simulated_time, sep=""))

dbDisconnect(conn)
dbUnloadDriver(drv)
